#!/bin/bash
if [ "$1" != "" ]
then
	echo "Creation du dossier resultat s'il n'existe pas"
	mkdir resultat
	echo "Creation du dossier resultat/images s'il n'existe pas"
	mkdir resultat/images
	
	echo "outv1.xml : Suppresion des CDATA"
	cat $1 | sed 's/<!\[CDATA\[//g' | sed 's/\]\]>//g' > outv1.xml

	echo "outv2.xml : Remplacement de <br> par <br/> et &nbsp; par un espace."
	cat outv1.xml | sed 's/<br>/<br\/>/g' | sed 's/&nbsp;/ /g' > outv2.xml

	echo "outv3.xml : Fermeture de la balise <img...>"
	cat outv2.xml | sed -r 's/<(img[^>]*)>/<\1\/>/gi' | sed -r 's/\/\/>/\/>/g' > outv3.xml

	echo "outv4.xml : Suppression des espaces dans les noms des images dans la balise <file name='...'>. Plusieurs passe nécessaires" 
	cp outv3.xml outv3tmp.xml
	cat outv3tmp.xml | sed -r 's/(<file\s+name\s*=\s*"[^\s"]*)\s+([^"]*)/\1_\2/gi' | sed -r 's/(<file\s+name\s*=\s*"[^\s"]*)\s+([^"]*)/\1_\2/gi' > outv4.xml
	mv -f outv4.xml outv3tmp.xml
	cat outv3tmp.xml | sed -r 's/(<file\s+name\s*=\s*"[^\s"]*)\s+([^"]*)/\1_\2/gi' | sed -r 's/(<file\s+name\s*=\s*"[^\s"]*)\s+([^"]*)/\1_\2/gi' > outv4.xml
	mv -f outv4.xml outv3tmp.xml
	cat outv3tmp.xml | sed -r 's/(<file\s+name\s*=\s*"[^\s"]*)\s+([^"]*)/\1_\2/gi' | sed -r 's/(<file\s+name\s*=\s*"[^\s"]*)\s+([^"]*)/\1_\2/gi' > outv4.xml
	rm -f outv3tmp.xml

	echo "outv5.xml : Suppression des %20 dans les noms des images dans la balise <img src='...'>. Plusieurs passe nécessaires" 
	cp outv4.xml outv4tmp.xml
	cat outv4tmp.xml | sed -r 's/(<img\s+src\s*=\s*"[^"]*)%20+([^"]*)/\1_\2/gi' | sed -r 's/(<img\s+src\s*=\s*"[^"]*)%20+([^"]*)/\1_\2/gi' > outv5.xml
	mv -f outv5.xml outv4tmp.xml
	cat outv4tmp.xml | sed -r 's/(<img\s+src\s*=\s*"[^"]*)%20+([^"]*)/\1_\2/gi' | sed -r 's/(<img\s+src\s*=\s*"[^"]*)%20+([^"]*)/\1_\2/gi' > outv5.xml
	mv -f outv5.xml outv4tmp.xml
	cat outv4tmp.xml | sed -r 's/(<img\s+src\s*=\s*"[^"]*)%20+([^"]*)/\1_\2/gi' | sed -r 's/(<img\s+src\s*=\s*"[^"]*)%20+([^"]*)/\1_\2/gi' > outv5.xml
	rm -f outv4tmp.xml

	echo "outv6.xml : Remplassement des balisages LaTeX \[...\],\(...\),$$...$$ par le balisage temporaire unique £...§"
	cat outv5.xml | sed -r 's/\$\$([^\$]*)\$\$/£\1§/g'  | sed -r 's/\\\(/£/g' | sed -r 's/\\\)/§/g'| sed -r 's/\\\[/£/g' | sed -r 's/\\\]/§/g' > outv6.xml 

	echo "outv7.xml : Remplacement des symboles & par &amp;"
	cat outv6.xml | sed -r 's/&([#\w\d]*);/¤\1;/g' | sed -r 's/&/&amp;/g' | sed -r 's/¤/&/g' > outv7.xml

	echo "outv8.xml : Concaténation des lignes là où les équations sont sur plusieurs lignes"
	cat outv7.xml | sed -r  ':a {N;  s/£([^§]*)\n/£\1/g ;  /§/!ba; P; D}'  > outv8.xml
	
	echo "outv9.xml : Remplacement des symboles < par &lt; dans les équations LaTeX"
	cp outv8.xml outv8tmp.xml
	cat outv8tmp.xml | sed -r 's/£([^<£§]*)(<)([^§]*)§/£\1\&lt;\3§/g' | sed -r 's/£([^<£§]*)(<)([^§]*)§/£\1\&lt;\3§/g' > outv9.xml
	mv -f outv9.xml outv8tmp.xml
	cat outv8tmp.xml | sed -r 's/£([^<£§]*)(<)([^§]*)§/£\1\&lt;\3§/g' | sed -r 's/£([^<£§]*)(<)([^§]*)§/£\1\&lt;\3§/g' > outv9.xml
	mv -f outv9.xml outv8tmp.xml
	cat outv8tmp.xml | sed -r 's/£([^<£§]*)(<)([^§]*)§/£\1\&lt;\3§/g' | sed -r 's/£([^<£§]*)(<)([^§]*)§/£\1\&lt;\3§/g' > outv9.xml
	rm -f outv8tmp.xml
	
	echo "outv10.xml : Remise en place du balisage des équations LaTeX (remplassement de £ par \( et § par \)"
	cat outv9.xml | sed -r 's/£/\\\(/g' | sed -r 's/§/\\\)/g' > outv10.xml

	echo "resultat : Exécution de la moulinette de transformation xmlMoodle -> Xml Opale/Topoze"
	saxonb-xslt -ext:on -o:resultat.xml -xsl:moulinette-opale.xsl -s:outv10.xml
	
	echo "resultat/*.quiz : Sortie des images inline pour en faire des images ressources Scenari"
	cd resultat
#	for f in *.quiz; do
#		cp -f $f tmp
#		cat tmp | sed -r 's/<sc:inlineImg role="ico"(.*)?\/>/<\/sc:para><\/op:txt><\/sp:txt><sp:res \1><op:resInfoM\/><\/sp:res><sp:txt><op:txt><sc:para>/g' > "$f"
#	done
	rm tmp
	cd ..
	
	
	
	exit 0
else
	echo "Syntaxe de la commande : moulinette.sh <xmlmoodle.xml>"
	exit 0
fi

