<xsl:stylesheet xmlns:sc="http://www.utc.fr/ics/scenari/v3/core"
					 xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive"
					 xmlns:op="utc.fr:ics/opale3"
					 xmlns:opa="kelis.fr:opa"
					 xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
					 xmlns:redirect="http://xml.apache.org/xalan/redirect"
					 xmlns:xs="http://www.w3.org/2001/XMLSchema"
					 xmlns:saxon="http://saxon.sf.net/"
					 xmlns:fos="java.io.FileOutputStream"
					 extension-element-prefixes="redirect" version="2.0">
	<xsl:output method="xml" indent="yes"/>

	<xsl:template match="quiz">
		<xsl:apply-templates select="question"/>
	</xsl:template>

	<xsl:template match="question">
		<xsl:variable name="questionname">
			<xsl:value-of select="name/text/text()"/>
		</xsl:variable>
		<xsl:variable name="nomFichier">
			<xsl:choose>
				<!-- Seuls les QCU/QCM et TRUEFALSE sont traités ! -->
				<xsl:when test="not(@type='multichoice' or @type='truefalse')">
					<xsl:value-of select="concat(substring-after(translate (preceding::comment()[position()=1], ' ', ''),'question:'),'_QCM_',@type,'_non_traite.quiz')"/>
				</xsl:when>
				<xsl:otherwise>
<!--					<xsl:value-of select="concat(substring-after(translate (preceding::comment()[position()=1], ' ', ''),'question:'),'.quiz')"/>-->
					<xsl:value-of select="concat(replace(translate($questionname,'áàâäéèêëíìîïóòôöúùûü','aaaaeeeeiiiioooouuuu'), '[^a-zA-Z0-9]', '-'),'.quiz')"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:result-document method="xml" href="resultat/{$nomFichier}">
			<xsl:variable name="questioncategory" select="substring-after(preceding::question[@type='category'][position()=1]/category/text, '$course$/')"/>
			<xsl:comment><comment><xsl:value-of select="$questioncategory"/></comment></xsl:comment>			
			<sc:item xmlns:sc="http://www.utc.fr/ics/scenari/v3/core">
				<xsl:choose>
					<xsl:when test="single='false'">
						<op:mcqMur xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive" xmlns:op="utc.fr:ics/opale3">
							<xsl:call-template name="contenuQuiz"/>
						</op:mcqMur>
					</xsl:when>
					<!-- TRUE/FALSE traité comme des QCU => attention plus loin il faut traiter les réponses différemment -->
					<xsl:when test="single='true' or @type='truefalse'">
						<op:mcqSur xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive" xmlns:op="utc.fr:ics/opale3">
							<xsl:call-template name="contenuQuiz"/>
							<xsl:if test="count(answer)>5">QCU avec plus de 5 options</xsl:if>
						</op:mcqSur>						
					</xsl:when>
					<xsl:otherwise>
						<XX_ITEM_INCONNU/>
					</xsl:otherwise>
				</xsl:choose>	
			</sc:item>
		</xsl:result-document>
	</xsl:template>

	<xsl:template name="contenuQuiz">
		<xsl:variable name="code">
			<xsl:call-template name="code"/>
		</xsl:variable>
		<op:exeM>
			<sp:title>
				<xsl:value-of select="name/text/text()"/>
			</sp:title>

			<!-- Besoin spécifique à l'export ver TOPOZE. Pour un export Opale, mettre en commentaire les 2 <xsl:if>, ci-dessous, traitant "$code" -->
			<xsl:if test="substring-before($code, '__')!=''">
				<sp:themeLycee><xsl:value-of select="substring-before($code, '__')"/></sp:themeLycee>
			</xsl:if>
			<xsl:if test="substring-after($code, '__')!=''">
				<sp:themeLicence><xsl:value-of select="substring-after($code, '__')"/></sp:themeLicence>
			</xsl:if>
			<xsl:apply-templates select="tags"/>						
		</op:exeM>
		<xsl:apply-templates select="questiontext"/>
		<sc:choices>
			<xsl:apply-templates select="answer"/>
		</sc:choices>
		<!-- Ajout de la bonne solution dans les 2 cas de QCU et TRUE/FALSE -->
		<xsl:if test="(single='true' or @type='truefalse') and answer[@fraction &gt; 0]">
			<xsl:variable name="bonneSol">
				<xsl:apply-templates select="answer[@fraction!=0]" mode="comptage"/>
				<xsl:if test="count(answer[@fraction!=0])!=1">XX_</xsl:if>
			</xsl:variable>
			<sc:solution choice="{$bonneSol}"/>
		</xsl:if>
		<xsl:apply-templates select="generalfeedback[text!='']"/>
	</xsl:template>

	<xsl:template match="answer[@fraction!=0]" mode="comptage">
		<xsl:value-of select="count(preceding-sibling::answer)+1"/>		
	</xsl:template>

	<xsl:template match="question[@type='category'] | name | br | file"/>
	
	<xsl:template match="questiontext">
		<sc:question>
			<op:res>
				<sp:txt>
					<xsl:apply-templates/>
				</sp:txt>
			</op:res>
		</sc:question>
	</xsl:template>
    
	<xsl:template match="answer">
		<sc:choice>
			<xsl:choose>
				<xsl:when test="../single='false'">
					<xsl:choose>
						<xsl:when test="round(@fraction) &gt; 0">
							<xsl:attribute name="solution">checked</xsl:attribute>
						</xsl:when>
						<xsl:otherwise>
							<xsl:attribute name="solution">unchecked</xsl:attribute>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:when>
			</xsl:choose>
			<sc:choiceLabel>
				<xsl:apply-templates select="text"/>
			</sc:choiceLabel>
			<xsl:if test="feedback/text!=''">
				<sc:choiceExplanation>
					<xsl:apply-templates select="feedback/text"/>
				</sc:choiceExplanation>
			</xsl:if>
		</sc:choice>
	</xsl:template>

	<xsl:template match="generalfeedback">
		<sc:globalExplanation>
			<op:res>
				<sp:txt>
					<xsl:apply-templates/>
				</sp:txt>
			</op:res>
		</sc:globalExplanation>
	</xsl:template>

	<xsl:template match="text">
		<op:txt>
			<xsl:for-each select="child::node()">
				<xsl:choose>
					<!-- Ici l'ajout du <sc:para> est  géré par le <xsl:template match="p">	-->
					<xsl:when test="self::p">
						<xsl:apply-templates select="."/>
					</xsl:when>
					<!-- Regroupe 2 cas rencontrés : les cas de texte isolé en présence mais hors de balise <p> et les cas d'absence de <p> -->
					<xsl:when test="self::text()">
						<sc:para xml:space="preserve">
							<xsl:apply-templates select="."/>
						</sc:para>
					</xsl:when>
					<xsl:otherwise>
						<sc:para xml:space="preserve">
							<xsl:apply-templates select="."/>							
						</sc:para>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:for-each>
		</op:txt>
	</xsl:template>

	<xsl:template match="tags">
		<sp:info>
			<op:info>
				<sp:keywds>
					<op:keywds>
						<xsl:apply-templates mode="keywords"/>
					</op:keywds>
				</sp:keywds>
				<xsl:apply-templates mode="origine"/>
			</op:info>
		</sp:info>
	</xsl:template>

	<xsl:template match="tag" mode="origine">
		<xsl:choose>
			<xsl:when test="contains(text, 'origine:')">
				<sp:cpyrgt>
					<op:sPara>
						<sc:para xml:space="preserve">
							<sc:phrase role="url"><op:urlM xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive" xmlns:op="utc.fr:ics/opale3" xmlns:sc="http://www.utc.fr/ics/scenari/v3/core">
								<sp:url>http://<xsl:value-of select="substring-after(text, 'origine:')"/></sp:url></op:urlM>
								<xsl:value-of select="substring-after(text, 'origine:')"/>						
							</sc:phrase>
						</sc:para>
					</op:sPara>
				</sp:cpyrgt>
			</xsl:when>
		</xsl:choose>
	</xsl:template>
	<xsl:template match="tag" mode="keywords">
		<xsl:choose>
			<xsl:when test="not(contains(text, 'origine:'))">
				<sp:keywd>
					<xsl:value-of select="text"/>
				</sp:keywd>
			</xsl:when>
		</xsl:choose>
	</xsl:template>

	<xsl:template match="p">
		<xsl:choose>
			<xsl:when test="child::text()">
				<xsl:choose>
					<xsl:when test="local-name(parent::node())='p'">
						<xsl:apply-templates/>
					</xsl:when>
					<xsl:otherwise>
						<sc:para xml:space="preserve">
							<xsl:apply-templates/>
						</sc:para>
					</xsl:otherwise>	
				</xsl:choose>
			</xsl:when>
			<xsl:otherwise>
				<sc:para xml:space="preserve">
					<xsl:apply-templates/>
				</sc:para>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template match="text()">
	<xsl:choose>
			<!-- Cas ou le Latex dans Moodle est balisé par '\( ' et '\)' -->
			<xsl:when test="contains(., '\(')">
				<xsl:call-template name="latex">
					<xsl:with-param name="chaine" select="."/>
				</xsl:call-template>
			</xsl:when>
			<!-- Cas ou le Latex dans Moodle est balisé par '$$ ' et '$$' -->
			<xsl:when test="contains(., '$$')">
				<xsl:call-template name="latex">
					<xsl:with-param name="chaine" select="."/>
				</xsl:call-template>
			</xsl:when>
			<xsl:otherwise>
				<xsl:choose>
					<!-- Traitement spécifique des réponses aux question TRUE/FALSE (remplacement de true/false par vrai/faux) -->
					<xsl:when test="ancestor::question[@type='truefalse'] and ancestor::answer and matches(.,'^true$')">vrai</xsl:when>
					<xsl:when test="ancestor::question[@type='truefalse'] and ancestor::answer and matches(.,'^false$')">faux</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="."/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<!-- Traitement des équations LaTeX. 
		ATTENTION ! 
		Ce template xsl ne traite pas les cas ou dans le même bloc de texte il y aurait mélange dans le balisage LaTeX $$...$$ et \(...\)
	-->
	<xsl:template name="latex">
		<xsl:param name="chaine"/>
		<xsl:choose>
			<!-- Cas ou le Latex dans Moodle est balisé par '\( ' et '\)' -->
			<xsl:when test="contains($chaine, '\(')">
				<xsl:value-of select="substring-before($chaine, '\(')"/>
				<sc:textLeaf role="mathtex">
					<xsl:value-of select="substring-before(substring-after($chaine, '\('), '\)')"/>
				</sc:textLeaf>
				<xsl:call-template name="latex">
					<xsl:with-param name="chaine" select="substring-after($chaine, '\)')"/>
				</xsl:call-template>
			</xsl:when>
			<!-- Cas ou le Latex dans Moodle est balisé par '$$ ' et '$$' -->
			<xsl:when test="contains($chaine, '$$')">
				<xsl:value-of select="substring-before($chaine, '$$')"/>
				<sc:textLeaf role="mathtex">
					<xsl:value-of select="substring-before(substring-after($chaine, '$$'), '$$')"/>
				</sc:textLeaf>
				<xsl:call-template name="latex">
					<xsl:with-param name="chaine" select="substring-after(substring-after($chaine, '$$'), '$$')"/>
				</xsl:call-template>
			</xsl:when>
			<xsl:otherwise>
				<!-- Ramassage de la fin de la chaine -->
				<xsl:value-of select="$chaine"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
		
	<xsl:template match="img">
		<xsl:variable name="nomImage" select="substring-after(@src, '@@PLUGINFILE@@/')"/>
		<xsl:choose>
			<xsl:when test="name(..)='p'">
				<sc:inlineImg role="ico" sc:refUri="{concat('images/',$nomImage)}"/>
			</xsl:when>
			<xsl:when test="name(..)='text'">
				<sc:inlineImg role="ico" sc:refUri="{concat('images/',$nomImage)}"/>
			</xsl:when>
			<xsl:otherwise>
				<sc:para xml:space="preserve">
					<sc:inlineImg role="ico" sc:refUri="{concat('images/',$nomImage)}"/>
				</sc:para>
			</xsl:otherwise>
		</xsl:choose>
		<xsl:variable name="fos" select="fos:new(string(concat('resultat/images/',$nomImage)))"/>
		<xsl:value-of select="fos:write($fos,saxon:base64Binary-to-octets(xs:base64Binary(following::file[@name=$nomImage][1])))"/>
	   <xsl:value-of select="fos:close($fos)"/>
	</xsl:template>

	<xsl:template match="em | strong | b">
		<sc:inlineStyle role="emp">
			<xsl:apply-templates/>
		</sc:inlineStyle>
	</xsl:template>

	<xsl:template match="sub">
		<sc:textLeaf role="ind">
			<xsl:apply-templates/>
		</sc:textLeaf>
	</xsl:template>

	<xsl:template match="span | div">
		<xsl:apply-templates/>
	</xsl:template>

	<xsl:template match="sup">
		<sc:textLeaf role="exp">
			<xsl:apply-templates/>
		</sc:textLeaf>
	</xsl:template>

	<xsl:template match="a">
		<sc:phrase role="url">
			<op:urlM xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive" xmlns:op="utc.fr:ics/opale3" xmlns:sc="http://www.utc.fr/ics/scenari/v3/core">
				<sp:url>
					<xsl:value-of select="@href"/>
				</sp:url>
			</op:urlM>
			<xsl:apply-templates/>
		</sc:phrase>
	</xsl:template>

	<xsl:template match="ul">
		<sc:itemizedList>
			<xsl:apply-templates/>
		</sc:itemizedList>
	</xsl:template>

	<xsl:template match="ol">
		<sc:orderedList>
			<xsl:apply-templates/>
		</sc:orderedList>
	</xsl:template>

	<xsl:template match="li">
		<sc:listItem>
			<xsl:choose>
				<xsl:when test="p">
					<xsl:apply-templates/>
				</xsl:when>
				<xsl:otherwise>
					<sc:para xml:space="preserve">
						<xsl:apply-templates/>							
					</sc:para>
				</xsl:otherwise>
			</xsl:choose>
		</sc:listItem>
	</xsl:template>

	<xsl:template match="table">
		<sc:table role="">
			<xsl:apply-templates select="tbody/tr"/>
		</sc:table>
	</xsl:template>

	<xsl:template match="tr">
		<sc:row>
			<xsl:apply-templates/>
		</sc:row>
	</xsl:template>

	<xsl:template match="td">
		<sc:cell>
			<xsl:choose>
				<xsl:when test="contains(@style, 'text-align: center')">
					<xsl:attribute name="role">word</xsl:attribute>
				</xsl:when>
				<xsl:when test="contains(@style, 'text-align: right')">
					<xsl:attribute name="role">num</xsl:attribute>
				</xsl:when>
			</xsl:choose>
			<xsl:apply-templates/>
		</sc:cell>
	</xsl:template>
	
	<xsl:template match="*">
		<nongere>
			<xsl:value-of select="local-name()"/>
		</nongere>
	</xsl:template>

	<xsl:template name="code">
		<xsl:variable name="intitule" select="substring-after(preceding::question[@type='category'][position()=1]/category/text, '$course$/')"/>
		<xsl:choose>
			<xsl:when test="$intitule='Chimie'">chim__chim</xsl:when>
			<xsl:when test="$intitule='Chimie/Agir : défis du XXIe siècle'">chim-agir__chim-electrochim</xsl:when>
			<xsl:when test="$intitule='Chimie/Agir : défis du XXIe siècle/Convertir l''énergie et économiser les ressources'">chim-agir-convener__chim-electrochim</xsl:when>
			<xsl:when test="$intitule='Chimie/Agir : défis du XXIe siècle/Convertir l''énergie et économiser les ressources/énergie libérée lors de la combustion d''un hydrocarbure ou d''un alcool: écrire une équation de combustion'">chim-agir-convener-enerlib__chim-electrochim</xsl:when>
			<xsl:when test="$intitule='Chimie/Agir : défis du XXIe siècle/Convertir l''énergie et économiser les ressources/oxydant, réducteur, couple oxydant//réducteur'">chim-agir-convener-oxydred__chim-electrochim</xsl:when>
			<xsl:when test="$intitule='Chimie/Agir : défis du XXIe siècle/Convertir l''énergie et économiser les ressources/piles et accumulateurs'">chim-agir-convener-pilesaccu__chim-electrochim</xsl:when>
			<xsl:when test="$intitule='Chimie/Agir : défis du XXIe siècle/Convertir l''énergie et économiser les ressources/réaction d''oxydo-réduction'">chim-agir-convener-reacoxydo__chim-electrochim</xsl:when>
			<xsl:when test="$intitule='Chimie/Agir : défis du XXIe siècle/Convertir l''énergie et économiser les ressources/stockage et conversion de l''énergie chimique'">chim-agir-convener-stockconv__chim-electrochim</xsl:when>
			<xsl:when test="$intitule='Chimie/Agir : défis du XXIe siècle/Économiser les ressources et respecter l''environnement'">chim-agir-econres__chim-electrochim</xsl:when>
			<xsl:when test="$intitule='Chimie/Agir : défis du XXIe siècle/Économiser les ressources et respecter l''environnement/dosages direct et indirect'">chim-agir-econres-dosdir__chim-electrochim</xsl:when>
			<xsl:when test="$intitule='Chimie/Agir : défis du XXIe siècle/Économiser les ressources et respecter l''environnement/équivalence dans un titrage pH-métrique, conductimétrique'">chim-agir-econres-equivtit__chim-electrochim</xsl:when>
			<xsl:when test="$intitule='Chimie/Agir : défis du XXIe siècle/Synthétiser des molécules, fabriquer de nouveaux matériaux'">chim-agir-synmol__chim-chimsolu</xsl:when>
			<xsl:when test="$intitule='Chimie/Agir : défis du XXIe siècle/Synthétiser des molécules, fabriquer de nouveaux matériaux/acides carboxyliques: nomenclature, caractère acide, solubilité et pH'">chim-agir-synmol-accarbo__chim-chimsolu</xsl:when>
			<xsl:when test="$intitule='Chimie/Agir : défis du XXIe siècle/Synthétiser des molécules, fabriquer de nouveaux matériaux/alcools, aldhéhydes, cétones: nomenclature, oxydation'">chim-agir-synmol-alcald__chim-chimsolu</xsl:when>
			<xsl:when test="$intitule='Chimie/Agir : défis du XXIe siècle/Synthétiser des molécules, fabriquer de nouveaux matériaux/Obtention d''un acide carboxylique ou d''une cétone ; rendement d''une synthèse.'">chim-agir-synmol-obtacid__chim-chimsolu</xsl:when>
			<xsl:when test="$intitule='Chimie/Agir : défis du XXIe siècle/Synthétiser des molécules, fabriquer de nouveaux matériaux/protocole de synthèse organique: réactifs, solvants, catalyseur, produits, rendement, sécurité, coût'">chim-agir-synmol-protosyn__chim-chimsolu</xsl:when>
			<xsl:when test="$intitule='Chimie/Comprendre : lois et modèles'">chim-comploismod__chim-structransfmat</xsl:when>
			<xsl:when test="$intitule='Chimie/Comprendre : lois et modèles/Cohésion et transformations de la matière'">chim-comploismod-cohtransf__chim-structransfmat</xsl:when>
			<xsl:when test="$intitule='Chimie/Comprendre : lois et modèles/Cohésion et transformations de la matière/aspects énergétiques d''une variation de température et un changement d''état'">chim-comploismod-cohtransf-aspener__chim-structransfmat</xsl:when>
			<xsl:when test="$intitule='Chimie/Comprendre : lois et modèles/Cohésion et transformations de la matière/charge élémentaire e'">chim-comploismod-cohtransf-charele__chim-structransfmat</xsl:when>
			<xsl:when test="$intitule='Chimie/Comprendre : lois et modèles/Cohésion et transformations de la matière/cohésion du noyau, stabilité'">chim-comploismod-cohtransf-cohenoy__chim-structransfmat</xsl:when>
			<xsl:when test="$intitule='Chimie/Comprendre : lois et modèles/Cohésion et transformations de la matière/conservation de la matière lors d''une dissolution'">chim-comploismod-cohtransf-consmat__chim-structransfmat</xsl:when>
			<xsl:when test="$intitule='Chimie/Comprendre : lois et modèles/Cohésion et transformations de la matière/effet du caractère polaire d''un solvant lors d''une dissolution'">chim-comploismod-cohtransf-effcarac__chim-structransfmat</xsl:when>
			<xsl:when test="$intitule='Chimie/Comprendre : lois et modèles/Cohésion et transformations de la matière/électronégativité'">chim-comploismod-cohtransf-elecneg__chim-structransfmat</xsl:when>
			<xsl:when test="$intitule='Chimie/Comprendre : lois et modèles/Cohésion et transformations de la matière/fission et fusion'">chim-comploismod-cohtransf-fissfus__chim-structransfmat</xsl:when>
			<xsl:when test="$intitule='Chimie/Comprendre : lois et modèles/Cohésion et transformations de la matière/interactions fondamentales'">chim-comploismod-cohtransf-interfond__chim-structransfmat</xsl:when>
			<xsl:when test="$intitule='Chimie/Comprendre : lois et modèles/Cohésion et transformations de la matière/isotopie et isotopes'">chim-comploismod-cohtransf-isotisot__chim-structransfmat</xsl:when>
			<xsl:when test="$intitule='Chimie/Comprendre : lois et modèles/Cohésion et transformations de la matière/lois de conservation dans les réactions nucléaires'">chim-comploismod-cohtransf-loiscons__chim-structransfmat</xsl:when>
			<xsl:when test="$intitule='Chimie/Comprendre : lois et modèles/Cohésion et transformations de la matière/matière à différente échelle: les ordres de grandeur'">chim-comploismod-cohtransf-matdiff__chim-structransfmat</xsl:when>
			<xsl:when test="$intitule='Chimie/Comprendre : lois et modèles/Cohésion et transformations de la matière/nomenclature des alcanes et alcools; formule semi-developpée'">chim-comploismod-cohtransf-nomalc__chim-structransfmat</xsl:when>
			<xsl:when test="$intitule='Chimie/Comprendre : lois et modèles/Cohésion et transformations de la matière/particules élémentaires: électrons, neutrons, protons'">chim-comploismod-cohtransf-partelem__chim-structransfmat</xsl:when>
			<xsl:when test="$intitule='Chimie/Comprendre : lois et modèles/Cohésion et transformations de la matière/radioactivité naturelle et artificielle'">chim-comploismod-cohtransf-radionat__chim-structransfmat</xsl:when>
			<xsl:when test="$intitule='Chimie/Comprendre : lois et modèles/Cohésion et transformations de la matière/reactions nucléaires et aspects énergétiques associés'">chim-comploismod-cohtransf-reacnuc__chim-structransfmat</xsl:when>
			<xsl:when test="$intitule='Chimie/Comprendre : lois et modèles/Cohésion et transformations de la matière/solide moléculaire'">chim-comploismod-cohtransf-solmol__chim-structransfmat</xsl:when>
			<xsl:when test="$intitule='Chimie/Comprendre : lois et modèles/Énergie, matière et rayonnement'">chim-comploismod-enermatray__chim-structransfmat</xsl:when>
			<xsl:when test="$intitule='Chimie/Comprendre : lois et modèles/Énergie, matière et rayonnement/ordres de grandeur domaines micro//macroscopiques'">chim-comploismod-enermatray-ordgran__chim-structransfmat</xsl:when>
			<xsl:when test="$intitule='Chimie/Comprendre : lois et modèles/Structure et transformation de la matière'">chim-comploismod-strtran__chim-structransfmat</xsl:when>
			<xsl:when test="$intitule='Chimie/Comprendre : lois et modèles/Structure et transformation de la matière/acide fort et base forte'">chim-comploismod-strtran-acbas__chim-structransfmat</xsl:when>
			<xsl:when test="$intitule='Chimie/Comprendre : lois et modèles/Structure et transformation de la matière/carbone asymétrique'">chim-comploismod-strtran-carbasym__chim-structransfmat</xsl:when>
			<xsl:when test="$intitule='Chimie/Comprendre : lois et modèles/Structure et transformation de la matière/catégories de réaction: substitution, addition, élimination'">chim-comploismod-strtran-catreac__chim-structransfmat</xsl:when>
			<xsl:when test="$intitule='Chimie/Comprendre : lois et modèles/Structure et transformation de la matière/chiralité'">chim-comploismod-strtran-chiralite__chim-structransfmat</xsl:when>
			<xsl:when test="$intitule='Chimie/Comprendre : lois et modèles/Structure et transformation de la matière/conformation d''une molécule'">chim-comploismod-strtran-confmol__chim-structransfmat</xsl:when>
			<xsl:when test="$intitule='Chimie/Comprendre : lois et modèles/Structure et transformation de la matière/contrôle du pH dans un milieu biologique'">chim-comploismod-strtran-contph__chim-structransfmat</xsl:when>
			<xsl:when test="$intitule='Chimie/Comprendre : lois et modèles/Structure et transformation de la matière/couple acide-base'">chim-comploismod-strtran-coupacbas__chim-structransfmat</xsl:when>
			<xsl:when test="$intitule='Chimie/Comprendre : lois et modèles/Structure et transformation de la matière/échange et transfert de protons: flèches courbes'">chim-comploismod-strtran-echtransf__chim-structransfmat</xsl:when>
			<xsl:when test="$intitule='Chimie/Comprendre : lois et modèles/Structure et transformation de la matière/énantiomérie, mélange racémique, diastéréoisomérie'">chim-comploismod-strtran-enantmel__chim-structransfmat</xsl:when>
			<xsl:when test="$intitule='Chimie/Comprendre : lois et modèles/Structure et transformation de la matière/formule topologique des molécules organiques'">chim-comploismod-strtran-formtopo__chim-structransfmat</xsl:when>
			<xsl:when test="$intitule='Chimie/Comprendre : lois et modèles/Structure et transformation de la matière/pH: définition, mesure'">chim-comploismod-strtran-phdefmes__chim-structransfmat</xsl:when>
			<xsl:when test="$intitule='Chimie/Comprendre : lois et modèles/Structure et transformation de la matière/propriétés biologique et stéréoisomérie'">chim-comploismod-strtran-propbiolost__chim-structransfmat</xsl:when>
			<xsl:when test="$intitule='Chimie/Comprendre : lois et modèles/Structure et transformation de la matière/représentation de Cram'">chim-comploismod-strtran-represcram__chim-structransfmat</xsl:when>
			<xsl:when test="$intitule='Chimie/Comprendre : lois et modèles/Structure et transformation de la matière/théorie de Brönsted'">chim-comploismod-strtran-theobron__chim-structransfmat</xsl:when>
			<xsl:when test="$intitule='Chimie/Comprendre : lois et modèles/Temps, mouvement et évolution'">chim-comploismod-tpsmvtevo__chim-structransfmat</xsl:when>
			<xsl:when test="$intitule='Chimie/La pratique du sport'">chim-pratspor__chim-thermochim</xsl:when>
			<xsl:when test="$intitule='Chimie/La pratique du sport/La pression'">chim-pratspor-pression__chim-thermochim</xsl:when>
			<xsl:when test="$intitule='Chimie/La pratique du sport/La pression/loi de Boyle-Mariotte'">chim-pratspor-pression-loiboymar__chim-thermochim</xsl:when>
			<xsl:when test="$intitule='Chimie/La pratique du sport/Matériaux et molécules dans le sport'">chim-pratspor-matmolsport__chim-plymmat</xsl:when>
			<xsl:when test="$intitule='Chimie/La santé'">chim-sante__chim</xsl:when>
			<xsl:when test="$intitule='Chimie/La santé/Diagnostic médical'">chim-sante-diagmed__chim-chimsolu</xsl:when>
			<xsl:when test="$intitule='Chimie/La santé/Diagnostic médical/atome et noyau'">chim-sante-diagmed-atnoy__chim-structransfmat</xsl:when>
			<xsl:when test="$intitule='Chimie/La santé/Diagnostic médical/classification périodique des éléments'">chim-sante-diagmed-claspele__chim-structransfmat</xsl:when>
			<xsl:when test="$intitule='Chimie/La santé/Diagnostic médical/identification des ions'">chim-sante-diagmed-identions__chim-structransfmat</xsl:when>
			<xsl:when test="$intitule='Chimie/La santé/Diagnostic médical/la quantité de matière, son unité: la mole'">chim-sante-diagmed-quantmatmole__chim-structransfmat</xsl:when>
			<xsl:when test="$intitule='Chimie/La santé/Diagnostic médical/masse molaire moléculaire'">chim-sante-diagmed-masmolmol__chim-structransfmat</xsl:when>
			<xsl:when test="$intitule='Chimie/La santé/Médicaments'">chim-sante-medicam__chim-chimsolu</xsl:when>
			<xsl:when test="$intitule='Chimie/La santé/Médicaments/chimie de synthèse'">chim-sante-medicam-chimsyn__chim-chimsolu</xsl:when>
			<xsl:when test="$intitule='Chimie/La santé/Médicaments/densité, masse volumique'">chim-sante-medicam-densmasvol__chim-chimsolu</xsl:when>
			<xsl:when test="$intitule='Chimie/La santé/Médicaments/dilution d''une solution'">chim-sante-medicam-dilusol__chim-chimsolu</xsl:when>
			<xsl:when test="$intitule='Chimie/La santé/Médicaments/groupes caractéristiques'">chim-sante-medicam-groupcaract__chim-chimsolu</xsl:when>
			<xsl:when test="$intitule='Chimie/La santé/Médicaments/espèce chimique'">chim-sante-medicam-espchim__chim-chimsolu</xsl:when>
			<xsl:when test="$intitule='Chimie/La santé/Médicaments/réaction chimique (équation)'">chim-sante-medicam-reacchim__chim-chimsolu</xsl:when>
			<xsl:when test="$intitule='Chimie/La santé/Médicaments/système chimique'">chim-sante-medicam-systchim__chim-chimsolu</xsl:when>
			<xsl:when test="$intitule='Chimie/Mesures et incertitudes'">chim-mesinc__chim-chimexpetech</xsl:when>
			<xsl:when test="$intitule='Chimie/Mesures et incertitudes/Chiffres significatifs'">chim-mesinc-chifsign__chim-chimexpetech</xsl:when>
			<xsl:when test="$intitule='Chimie/Mesures et incertitudes/Expression d''un résultat de mesure avec son incertitude'">chim-mesinc-expresumes__chim-chimexpetech</xsl:when>
			<xsl:when test="$intitule='Chimie/Observer: couleur et images, ondes et matière'">chim-obscoulim__chim-spectrosco</xsl:when>
			<xsl:when test="$intitule='Chimie/Observer: couleur et images, ondes et matière/Analyse spectrale'">chim-obscoulim-anaspec__chim-spectrosco</xsl:when>
			<xsl:when test="$intitule='Chimie/Observer: couleur et images, ondes et matière/Analyse spectrale/spectres IR'">chim-obscoulim-anaspec-specir__chim-spectrosco</xsl:when>
			<xsl:when test="$intitule='Chimie/Observer: couleur et images, ondes et matière/Matières colorées'">chim-obscoulim-matcolo__chim-chimsolu</xsl:when>
			<xsl:when test="$intitule='Chimie/Observer: couleur et images, ondes et matière/Matières colorées/couleur d''un mélange'">chim-obscoulim-matcolo-coulmel__chim-chimsolu</xsl:when>
			<xsl:when test="$intitule='Chimie/Observer: couleur et images, ondes et matière/Matières colorées/dosage de solutions colorées par étalonnage'">chim-obscoulim-matcolo-dossolcolo__chim-chimsolu</xsl:when>
			<xsl:when test="$intitule='Chimie/Observer: couleur et images, ondes et matière/Matières colorées/formule de Lewis'">chim-obscoulim-matcolo-forlewis__chim-chimsolu</xsl:when>
			<xsl:when test="$intitule='Chimie/Observer: couleur et images, ondes et matière/Matières colorées/indicateurs colorés'">chim-obscoulim-matcolo-indiccolo__chim-chimsolu</xsl:when>
			<xsl:when test="$intitule='Chimie/Observer: couleur et images, ondes et matière/Matières colorées/isomérieZ//E'">chim-obscoulim-matcolo-isome__chim-chimsolu</xsl:when>
			<xsl:when test="$intitule='Chimie/Observer: couleur et images, ondes et matière/Matières colorées/loi de Beer-Lambert'">chim-obscoulim-matcolo-loibeer__chim-chimsolu</xsl:when>
			<xsl:when test="$intitule='Chimie/Observer: couleur et images, ondes et matière/Matières colorées/molécules à liaisons conjuguées'">chim-obscoulim-matcolo-molliaconj__chim-chimsolu</xsl:when>
			<xsl:when test="$intitule='Chimie/Observer: couleur et images, ondes et matière/Matières colorées/réactif limitant'">chim-obscoulim-matcolo-reaclimi__chim-chimsolu</xsl:when>
			<xsl:when test="$intitule='Mathématiques'">math__math</xsl:when>
			<xsl:when test="$intitule='Mathématiques/Algorithmique'">math-algorith</xsl:when>
			<xsl:when test="$intitule='Mathématiques/Algorithmique/Boucle et itérateur, instruction conditionnelle'">math-algorith-bouiter</xsl:when>
			<xsl:when test="$intitule='Mathématiques/Algorithmique/Instructions élémentaires : affectation, calcul, entrée, sortie'">math-algorith-instelemaff</xsl:when>
			<xsl:when test="$intitule='Mathématiques/Algorithmique/Langage naturel, langage symbolique'">math-algorith-lannatlansym</xsl:when>
			<xsl:when test="$intitule='Mathématiques/Algorithmique/Logique: connecteur et quantificateur'">math-algorith-logconquan</xsl:when>
			<xsl:when test="$intitule='Mathématiques/Analyse'">math-analyse__math</xsl:when>
			<xsl:when test="$intitule='Mathématiques/Analyse/Calculs de dérivées : compléments'">math-analyse-calcderiv__math-derivation</xsl:when>
			<xsl:when test="$intitule='Mathématiques/Analyse/Dérivation'">math-analyse-derivation__math-derivation</xsl:when>
			<xsl:when test="$intitule='Mathématiques/Analyse/Dérivation/Dérivée d''une somme, d''un produit et d''un quotient'">math-analyse-derivation-derivsomm__math-derivation</xsl:when>
			<xsl:when test="$intitule='Mathématiques/Analyse/Dérivation/Dérivée des fonctions usuelles'">math-analyse-derivation-derivfonusu__math-derivation</xsl:when>
			<xsl:when test="$intitule='Mathématiques/Analyse/Dérivation/Extremum d''une fonction'">math-analyse-derivation-extremumfonc__math-derivation</xsl:when>
			<xsl:when test="$intitule='Mathématiques/Analyse/Dérivation/Fonction dérivée'">math-analyse-derivation-foncderivee__math-derivation</xsl:when>
			<xsl:when test="$intitule='Mathématiques/Analyse/Dérivation/Lien entre signe de la dérivée et sens de variation'">math-analyse-derivation-liensignderiv__math-derivation</xsl:when>
			<xsl:when test="$intitule='Mathématiques/Analyse/Dérivation/Nombre dérivé d''une fonction en un point'">math-analyse-derivation-nbrderivfonc__math-derivation</xsl:when>
			<xsl:when test="$intitule='Mathématiques/Analyse/Dérivation/Tangente à la courbe représentative d''une fonction dérivable en un point'">math-analyse-derivation-tancourrepres__math-derivation</xsl:when>
			<xsl:when test="$intitule='Mathématiques/Analyse/Équations'">math-analyse-equations</xsl:when>
			<xsl:when test="$intitule='Mathématiques/Analyse/Équations/Résolution algébrique d''équation'">math-analyse-equations-resolalgequa</xsl:when>
			<xsl:when test="$intitule='Mathématiques/Analyse/Équations/Résolution graphique d''équation'">math-analyse-equations-resolgraphequa</xsl:when>
			<xsl:when test="$intitule='Mathématiques/Analyse/Études de fonctions'">math-analyse-etufonc__math-fonctions</xsl:when>
			<xsl:when test="$intitule='Mathématiques/Analyse/Études de fonctions/Fonction croissante'">math-analyse-etufonc-fonccroiss__math-fonctions</xsl:when>
			<xsl:when test="$intitule='Mathématiques/Analyse/Études de fonctions/Fonction décroissante'">math-analyse-etufonc-foncdecroiss__math-fonctions</xsl:when>
			<xsl:when test="$intitule='Mathématiques/Analyse/Études de fonctions/Fonctions de référence'">math-analyse-etufonc-foncderef__math-fonctions</xsl:when>
			<xsl:when test="$intitule='Mathématiques/Analyse/Études de fonctions/Fonctions homographiques'">math-analyse-etufonc-fonchomo__math-fonctions</xsl:when>
			<xsl:when test="$intitule='Mathématiques/Analyse/Études de fonctions/Fonctions polynomes de degré 2'">math-analyse-etufonc-foncpoly__math-fonctions</xsl:when>
			<xsl:when test="$intitule='Mathématiques/Analyse/Études de fonctions/Maximum, minimum d''une fonction sur un intervalle'">math-analyse-etufonc-maxminfoncinter__math-fonctions</xsl:when>
			<xsl:when test="$intitule='Mathématiques/Analyse/Études de fonctions/Sens de variation des fonctions'">math-analyse-etufonc-sensvarfonc__math-fonctions</xsl:when>
			<xsl:when test="$intitule='Mathématiques/Analyse/Expressions algébriques'">math-analyse-expalg__math-fonctions</xsl:when>
			<xsl:when test="$intitule='Mathématiques/Analyse/Expressions algébriques/Transformation d''expressions algébriques en vue d''une résolution d''un problème'">math-analyse-expalg-transfexpalg__math-fonctions</xsl:when>
			<xsl:when test="$intitule='Mathématiques/Analyse/Fonction exponentielle'">math-analyse-fonexp__math-fonctions</xsl:when>
			<xsl:when test="$intitule='Mathématiques/Analyse/Fonction logarithme népérien'">math-analyse-fonlog__math-fonctions</xsl:when>
			<xsl:when test="$intitule='Mathématiques/Analyse/Fonctions'">math-analyse-fonctions__math-fonctions</xsl:when>
			<xsl:when test="$intitule='Mathématiques/Analyse/Fonctions/Antécédent'">math-analyse-fonctions-antece__math-fonctions</xsl:when>
			<xsl:when test="$intitule='Mathématiques/Analyse/Fonctions/Courbe représentative'">math-analyse-fonctions-courbrepres__math-fonctions</xsl:when>
			<xsl:when test="$intitule='Mathématiques/Analyse/Fonctions/Image'">math-analyse-fonctions-image__math-fonctions</xsl:when>
			<xsl:when test="$intitule='Mathématiques/Analyse/Fonctions de référence'">math-analyse-fonderef__math-fonctions</xsl:when>
			<xsl:when test="$intitule='Mathématiques/Analyse/Fonctions de référence/Fonctions affines'">math-analyse-fonderef-foncaffin__math-fonctions</xsl:when>
			<xsl:when test="$intitule='Mathématiques/Analyse/Fonctions de référence/Fonctions linéaires'">math-analyse-fonderef-fonclin__math-fonctions</xsl:when>
			<xsl:when test="$intitule='Mathématiques/Analyse/Fonctions de référence/Variations de la fonction carré'">math-analyse-fonderef-varfonccarre__math-fonctions</xsl:when>
			<xsl:when test="$intitule='Mathématiques/Analyse/Fonctions de référence/Variations de la fonction inverse'">math-analyse-fonderef-varfoncinv__math-fonctions</xsl:when>
			<xsl:when test="$intitule='Mathématiques/Analyse/Fonctions sinus et cosinus'">math-analyse-fonsin__math-fonctions</xsl:when>
			<xsl:when test="$intitule='Mathématiques/Analyse/Inéquations'">math-analyse-ineq</xsl:when>
			<xsl:when test="$intitule='Mathématiques/Analyse/Inéquations/Résolution algébrique d''inéquations'">math-analyse-ineq-resolalgineq</xsl:when>
			<xsl:when test="$intitule='Mathématiques/Analyse/Inéquations/Résolution graphique d''inéquation'">math-analyse-ineq-resgraphineq</xsl:when>
			<xsl:when test="$intitule='Mathématiques/Analyse/Intégration'">math-analyse-integration__math-integration</xsl:when>
			<xsl:when test="$intitule='Mathématiques/Analyse/Intégration/Définition de l''intégrale d''une fonction continue et positive sur [a,b] comme aire sous la courbe'">math-analyse-integration-defintegfonc__math-integration</xsl:when>
			<xsl:when test="$intitule='Mathématiques/Analyse/Intégration/Intégrale d''une fonction continue de signe quelconque'">math-analyse-integration-intfonccont__math-integration</xsl:when>
			<xsl:when test="$intitule='Mathématiques/Analyse/Intégration/Linéarité, positivité, relation de Chasles'">math-analyse-integration-linposrel__math-integration</xsl:when>
			<xsl:when test="$intitule='Mathématiques/Analyse/Intégration/Primitive d''une fonction continue sur un intervalle'">math-analyse-integration-primfonccont__math-integration</xsl:when>
			<xsl:when test="$intitule='Mathématiques/Analyse/Intégration/Théorème : toute fonction continue sur un intervalle admet des primitives'">math-analyse-integration-theofonccont__math-integration</xsl:when>
			<xsl:when test="$intitule='Mathématiques/Analyse/Limite de suite, théorème des gendarmes'">math-analyse-limsuit</xsl:when>
			<xsl:when test="$intitule='Mathématiques/Analyse/Limites de fonctions'">math-analyse-limfonc</xsl:when>
			<xsl:when test="$intitule='Mathématiques/Analyse/Limites de fonctions/Asymptote parallèle à l''un des axes de coordonnées'">math-analyse-limfonc-asympra</xsl:when>
			<xsl:when test="$intitule='Mathématiques/Analyse/Limites de fonctions/Limite d''une somme, d''un produit, d''un quotient ou d''une composée de deux fonctions'">math-analyse-limfonc-limsomprod</xsl:when>
			<xsl:when test="$intitule='Mathématiques/Analyse/Limites de fonctions/Limite finie ou infinie d''une fonction à l''infini'">math-analyse-limfonc-limfininf</xsl:when>
			<xsl:when test="$intitule='Mathématiques/Analyse/Limites de fonctions/Limite infinie d''une fonction en un point'">math-analyse-limfonc-liminffonc</xsl:when>
			<xsl:when test="$intitule='Mathématiques/Analyse/Limites de fonctions/Limites et comparaison'">math-analyse-limfonc-limcomp</xsl:when>
			<xsl:when test="$intitule='Mathématiques/Analyse/Second degré, polynomes'">math-analyse-secdegpoly__math-polyno</xsl:when>
			<xsl:when test="$intitule='Mathématiques/Analyse/Second degré, polynomes/Équation du second degré, discriminant'">math-analyse-secdegpoly-equsegdeg__math-polyno</xsl:when>
			<xsl:when test="$intitule='Mathématiques/Analyse/Second degré, polynomes/Forme canonique d''une fonction polynôme de degré deux'">math-analyse-secdegpoly-formcanofonc__math-polyno</xsl:when>
			<xsl:when test="$intitule='Mathématiques/Analyse/Second degré, polynomes/Signe du trinome'">math-analyse-secdegpoly-signtrin__math-polyno</xsl:when>
			<xsl:when test="$intitule='Mathématiques/Analyse/Suites'">math-analyse-suites__math-suites</xsl:when>
			<xsl:when test="$intitule='Mathématiques/Analyse/Suites/Comportement à l''infini de la suite (q n ), q étant un nombre réel'">math-analyse-suites-compinfsuit__math-suites</xsl:when>
			<xsl:when test="$intitule='Mathématiques/Analyse/Suites/Limite finie ou infinie d''une suite'">math-analyse-suites-limfininfsuit__math-suites</xsl:when>
			<xsl:when test="$intitule='Mathématiques/Analyse/Suites/Modes de génération d''une suite numérique'">math-analyse-suites-modgensuit__math-suites</xsl:when>
			<xsl:when test="$intitule='Mathématiques/Analyse/Suites/Limites et comparaison'">math-analyse-suites-limcompar__math-suites</xsl:when>
			<xsl:when test="$intitule='Mathématiques/Analyse/Suites/Opérations sur les limites'">math-analyse-suites-opelimit__math-suites</xsl:when>
			<xsl:when test="$intitule='Mathématiques/Analyse/Suites/Raisonnement par récurrence'">math-analyse-suites-raisrecurr__math-suites</xsl:when>
			<xsl:when test="$intitule='Mathématiques/Analyse/Suites/Sens de variation d''une suite numérique'">math-analyse-suites-sensvarsuit__math-suites</xsl:when>
			<xsl:when test="$intitule='Mathématiques/Analyse/Suites/Suite majorée, minorée, bornée'">math-analyse-suites-suitmajminbor__math-suites</xsl:when>
			<xsl:when test="$intitule='Mathématiques/Analyse/Suites/Suites arithmétiques et suites géométriques'">math-analyse-suites-suitarithgeom__math-suites</xsl:when>
			<xsl:when test="$intitule='Mathématiques/Analyse/Trigonométrie'">math-analyse-trigonom__math-trigonometrie</xsl:when>
			<xsl:when test="$intitule='Mathématiques/Analyse/Trigonométrie/Définition du cosinus d''un nombre réel'">math-analyse-trigonom-defcosnb__math-trigonometrie</xsl:when>
			<xsl:when test="$intitule='Mathématiques/Analyse/Trigonométrie/Définition du sinus d''un nombre réel'">math-analyse-trigonom-defsinnb__math-trigonometrie</xsl:when>
			<xsl:when test="$intitule='Mathématiques/Analyse/Trigonométrie/Enroulement de la droite numérique sur le cercle trigonométrique'">math-analyse-trigonom-enrouldrnum__math-trigonometrie</xsl:when>
			<xsl:when test="$intitule='Mathématiques/Géométrie'">math-geo__math-geo</xsl:when>
			<xsl:when test="$intitule='Mathématiques/Géométrie/Barycentre'">math-geo-barycen__math-geo</xsl:when>
			<xsl:when test="$intitule='Mathématiques/Géométrie/Configurations du plan'">math-geo-confplan__math-geo</xsl:when>
			<xsl:when test="$intitule='Mathématiques/Géométrie/Configurations du plan/Cercles'">math-geo-confplan-cercles__math-geo</xsl:when>
			<xsl:when test="$intitule='Mathématiques/Géométrie/Configurations du plan/Quadrilatères'">math-geo-confplan-quadri__math-geo</xsl:when>
			<xsl:when test="$intitule='Mathématiques/Géométrie/Configurations du plan/Triangles'">math-geo-confplan-triang__math-geo</xsl:when>
			<xsl:when test="$intitule='Mathématiques/Géométrie/Coordonnées d''un point du plan, repérage cartésien et polaire'">math-geo-coordplan__math-geo</xsl:when>
			<xsl:when test="$intitule='Mathématiques/Géométrie/Coordonnées d''un point du plan, repérage cartésien et polaire/Abscisse et ordonnée d''un point dans le plan rapporté à un repère orthonormé'">math-geo-coordplan-abscordpt__math-geo</xsl:when>
			<xsl:when test="$intitule='Mathématiques/Géométrie/Coordonnées d''un point du plan, repérage cartésien et polaire/Distance de deux points du plan'">math-geo-coordplan-distptspl__math-geo</xsl:when>
			<xsl:when test="$intitule='Mathématiques/Géométrie/Coordonnées d''un point du plan, repérage cartésien et polaire/Milieu d''un segment'">math-geo-coordplan-milsegmt__math-geo</xsl:when>
			<xsl:when test="$intitule='Mathématiques/Géométrie/Droites'">math-geo-droites__math-geo</xsl:when>
			<xsl:when test="$intitule='Mathématiques/Géométrie/Droites/Droite comme courbe représentative d''une fonction affine'">math-geo-droites-drcourbrepfonc__math-geo</xsl:when>
			<xsl:when test="$intitule='Mathématiques/Géométrie/Droites/Droites parallèles'">math-geo-droites-drparall__math-geo</xsl:when>
			<xsl:when test="$intitule='Mathématiques/Géométrie/Droites/Droites sécantes'">math-geo-droites-drsecan__math-geo</xsl:when>
			<xsl:when test="$intitule='Mathématiques/Géométrie/Droites/Équations de droites'">math-geo-droites-equdroit__math-geo</xsl:when>
			<xsl:when test="$intitule='Mathématiques/Géométrie/Géométrie dans l''espace'">math-geo-geomespace__math-geo</xsl:when>
			<xsl:when test="$intitule='Mathématiques/Géométrie/Géométrie dans l''espace/Cône et cylindre de révolution'">math-geo-geomespace-concylrevo__math-geo</xsl:when>
			<xsl:when test="$intitule='Mathématiques/Géométrie/Géométrie dans l''espace/droites et plan: intersection et parallélisme'">math-geo-geomespace-drplinterpara__math-geo</xsl:when>
			<xsl:when test="$intitule='Mathématiques/Géométrie/Géométrie dans l''espace/Droites et plans parallèles'">math-geo-geomespace-drplpara__math-geo</xsl:when>
			<xsl:when test="$intitule='Mathématiques/Géométrie/Géométrie dans l''espace/Droites et plans, positions relatives'">math-geo-geomespace-drplposrel__math-geo</xsl:when>
			<xsl:when test="$intitule='Mathématiques/Géométrie/Géométrie dans l''espace/géométrie vectorielle'">math-geo-geomespace-geomvecto__math-geo</xsl:when>
			<xsl:when test="$intitule='Mathématiques/Géométrie/Géométrie dans l''espace/orthogonalité'">math-geo-geomespace-orthog__math-geo</xsl:when>
			<xsl:when test="$intitule='Mathématiques/Géométrie/Géométrie dans l''espace/Parallélépipède rectangle'">math-geo-geomespace-parallrect__math-geo</xsl:when>
			<xsl:when test="$intitule='Mathématiques/Géométrie/Géométrie dans l''espace/produit scalaire'">math-geo-geomespace-prodscal__math-geo</xsl:when>
			<xsl:when test="$intitule='Mathématiques/Géométrie/Géométrie dans l''espace/Pyramides'">math-geo-geomespace-pyramides__math-geo</xsl:when>
			<xsl:when test="$intitule='Mathématiques/Géométrie/Géométrie dans l''espace/Sphère'">math-geo-geomespace-sphere__math-geo</xsl:when>
			<xsl:when test="$intitule='Mathématiques/Géométrie/Géométrie plane, transformations'">math-geo-geopltran__math-geo</xsl:when>
			<xsl:when test="$intitule='Mathématiques/Géométrie/Géométrie plane, transformations/Condition de colinéarité de deux vecteurs'">math-geo-geopltran-condcolvect__math-geo</xsl:when>
			<xsl:when test="$intitule='Mathématiques/Géométrie/Géométrie plane, transformations/Équation cartésienne d''une droite'">math-geo-geopltran-equcartdr__math-geo</xsl:when>
			<xsl:when test="$intitule='Mathématiques/Géométrie/Géométrie plane, transformations/Expression d''un vecteur du plan en fonction de deux vecteurs non colinéaires'">math-geo-geopltran-exprvectplan__math-geo</xsl:when>
			<xsl:when test="$intitule='Mathématiques/Géométrie/Géométrie plane, transformations/Vecteur directeur d''une droite'">math-geo-geopltran-vectdirdr__math-geo</xsl:when>
			<xsl:when test="$intitule='Mathématiques/Géométrie/Lieu géométrique'">math-geo-lieugeom__math-geo</xsl:when>
			<xsl:when test="$intitule='Mathématiques/Géométrie/Nombres complexes'">math-geo-nbcomplex__math-nbreelscompl</xsl:when>
			<xsl:when test="$intitule='Mathématiques/Géométrie/Nombres complexes/Affixe d''un point, d''un vecteur'">math-geo-nbcomplex-affptvect__math-nbreelscompl</xsl:when>
			<xsl:when test="$intitule='Mathématiques/Géométrie/Nombres complexes/Équation du second degré à coefficients réels'">math-geo-nbcomplex-equasecdeg__math-nbreelscompl</xsl:when>
			<xsl:when test="$intitule='Mathématiques/Géométrie/Nombres complexes/Forme algébrique, conjugué'">math-geo-nbcomplex-formalgconj__math-nbreelscompl</xsl:when>
			<xsl:when test="$intitule='Mathématiques/Géométrie/Nombres complexes/Forme trigonométrique : module et argument, interprétation géométrique ; notation exponentielle'">math-geo-nbcomplex-formtrigomod__math-nbreelscompl</xsl:when>
			<xsl:when test="$intitule='Mathématiques/Géométrie/Nombres complexes/Représentation géométrique'">math-geo-nbcomplex-represgeom__math-nbreelscompl</xsl:when>
			<xsl:when test="$intitule='Mathématiques/Géométrie/Nombres complexes/Somme, produit, quotient'">math-geo-nbcomplex-somprodquot__math-nbreelscompl</xsl:when>
			<xsl:when test="$intitule='Mathématiques/Géométrie/Produit scalaire'">math-geo-prodscal__math-geo</xsl:when>
			<xsl:when test="$intitule='Mathématiques/Géométrie/Produit scalaire/Application du produit scalaire'">math-geo-prodscal-appprodscal__math-geo</xsl:when>
			<xsl:when test="$intitule='Mathématiques/Géométrie/Produit scalaire/Calcul d''angles et de longueurs'">math-geo-prodscal-calcanglong__math-geo</xsl:when>
			<xsl:when test="$intitule='Mathématiques/Géométrie/Produit scalaire/Définition, propriétés'">math-geo-prodscal-defprop__math-geo</xsl:when>
			<xsl:when test="$intitule='Mathématiques/Géométrie/Produit scalaire/équation cartésienne d''un plan'">math-geo-prodscal-equacartpl__math-geo</xsl:when>
			<xsl:when test="$intitule='Mathématiques/Géométrie/Produit scalaire/Formules d''addition et de duplication des cosinus et sinus'">math-geo-prodscal-formadddupl__math-geo</xsl:when>
			<xsl:when test="$intitule='Mathématiques/Géométrie/Produit scalaire/Produit scalaire'">math-geo-prodscal-prodscal__math-geo</xsl:when>
			<xsl:when test="$intitule='Mathématiques/Géométrie/Produit scalaire/vecteur normal à un plan'">math-geo-prodscal-vectnormpl__math-geo</xsl:when>
			<xsl:when test="$intitule='Mathématiques/Géométrie/Produit scalaire/Vecteur normal à une droite'">math-geo-prodscal-vectnormdr__math-geo</xsl:when>
			<xsl:when test="$intitule='Mathématiques/Géométrie/Trigonométrie'">math-geo-trigonometrie__math-trigonometrie</xsl:when>
			<xsl:when test="$intitule='Mathématiques/Géométrie/Trigonométrie/Cercle trigonométrique'">math-geo-trigonometrie-cerctrigo__math-trigonometrie</xsl:when>
			<xsl:when test="$intitule='Mathématiques/Géométrie/Trigonométrie/Mesure d''un angle orienté, mesure principale'">math-geo-trigonometrie-mesangorient__math-trigonometrie</xsl:when>
			<xsl:when test="$intitule='Mathématiques/Géométrie/Trigonométrie/Radian'">math-geo-trigonometrie-radian__math-trigonometrie</xsl:when>
			<xsl:when test="$intitule='Mathématiques/Géométrie/Vecteurs'">math-geo-vecteurs__math-geo</xsl:when>
			<xsl:when test="$intitule='Mathématiques/Géométrie/Vecteurs/Coordonnés d''un vecteur dans un repère'">math-geo-vecteurs-coordvectrep__math-geo</xsl:when>
			<xsl:when test="$intitule='Mathématiques/Géométrie/Vecteurs/Définition de la translation qui transforme un point A du plan en un point B'">math-geo-vecteurs-deftransltransf__math-geo</xsl:when>
			<xsl:when test="$intitule='Mathématiques/Géométrie/Vecteurs/Égalité de deux vecteurs'">math-geo-vecteurs-egalvect__math-geo</xsl:when>
			<xsl:when test="$intitule='Mathématiques/Géométrie/Vecteurs/Produites d''un vecteur par un nombre réel'">math-geo-vecteurs-prodvectnbree__math-geo</xsl:when>
			<xsl:when test="$intitule='Mathématiques/Géométrie/Vecteurs/Relation de Chasles'">math-geo-vecteurs-relchasles__math-geo</xsl:when>
			<xsl:when test="$intitule='Mathématiques/Géométrie/Vecteurs/Somme de deux vecteurs'">math-geo-vecteurs-somvecteurs__math-geo</xsl:when>
			<xsl:when test="$intitule='Mathématiques/Géométrie dans l''espace'">math-geoesp__math-geo</xsl:when>
			<xsl:when test="$intitule='Mathématiques/Géométrie dans l''espace/Lieux géométriques'">math-geoesp-lieuxgeom__math-geo</xsl:when>
			<xsl:when test="$intitule='Mathématiques/Géométrie dans l''espace/Reconnaître un plan et une droite de l''espace'">math-geoesp-reconpldr__math-geo</xsl:when>
			<xsl:when test="$intitule='Mathématiques/Logique'">math-logique__math-lograis</xsl:when>
			<xsl:when test="$intitule='Mathématiques/Probabilités et statistiques'">math-probastat__math-stat</xsl:when>
			<xsl:when test="$intitule='Mathématiques/Probabilités et statistiques/Conditionnement, indépendance'">math-probastat-condindep__math-stat</xsl:when>
			<xsl:when test="$intitule='Mathématiques/Probabilités et statistiques/Conditionnement, indépendance/Conditionnement par un événement de probabilité non nulle'">math-probastat-condindep-condeven__math-stat</xsl:when>
			<xsl:when test="$intitule='Mathématiques/Probabilités et statistiques/Conditionnement, indépendance/Indépendance de deux événements'">math-probastat-condindep-indepeven__math-stat</xsl:when>
			<xsl:when test="$intitule='Mathématiques/Probabilités et statistiques/Échantillonnage'">math-probastat-echantil__math-stat</xsl:when>
			<xsl:when test="$intitule='Mathématiques/Probabilités et statistiques/Échantillonnage/Intervalle de fluctuation d''une fréquence au seuil de 95%'">math-probastat-echantil-intechant__math-stat</xsl:when>
			<xsl:when test="$intitule='Mathématiques/Probabilités et statistiques/Échantillonnage/Notion d''échantillon'">math-probastat-echantil-notechant__math-stat</xsl:when>
			<xsl:when test="$intitule='Mathématiques/Probabilités et statistiques/Échantillonnage/Réalisation d''une simulation'">math-probastat-echantil-realsimu__math-stat</xsl:when>
			<xsl:when test="$intitule='Mathématiques/Probabilités et statistiques/Échantillonnage/utilisation de la loi binomiale pour une prise de décision à partir d''une fréquence'">math-probastat-echantil-utilloibinom__math-stat</xsl:when>
			<xsl:when test="$intitule='Mathématiques/Probabilités et statistiques/Espérance, variance et écart-type'">math-probastat-espvarecar__math-stat</xsl:when>
			<xsl:when test="$intitule='Mathématiques/Probabilités et statistiques/Espérance, variance et écart-type/Loi binomiale'">math-probastat-espvarecar-loibinom__math-stat</xsl:when>
			<xsl:when test="$intitule='Mathématiques/Probabilités et statistiques/Espérance, variance et écart-type/Loi exponentielle'">math-probastat-espvarecar-loiexpo__math-stat</xsl:when>
			<xsl:when test="$intitule='Mathématiques/Probabilités et statistiques/Espérance, variance et écart-type/Loi normale'">math-probastat-espvarecar-loinorm__math-stat</xsl:when>
			<xsl:when test="$intitule='Mathématiques/Probabilités et statistiques/Espérance, variance et écart-type/Loi uniforme'">math-probastat-espvarecar-loiunif__math-stat</xsl:when>
			<xsl:when test="$intitule='Mathématiques/Probabilités et statistiques/Estimation'">math-probastat-estima__math-stat</xsl:when>
			<xsl:when test="$intitule='Mathématiques/Probabilités et statistiques/Estimation/intervalle de confiance'">math-probastat-estima-intconf__math-stat</xsl:when>
			<xsl:when test="$intitule='Mathématiques/Probabilités et statistiques/Estimation/niveau de confiance'">math-probastat-estima-nivconf__math-stat</xsl:when>
			<xsl:when test="$intitule='Mathématiques/Probabilités et statistiques/Modèle de la répétition d''expériences identiques et indépendantes à deux ou trois issues'">math-probastat-modrepetexp__math-stat</xsl:when>
			<xsl:when test="$intitule='Mathématiques/Probabilités et statistiques/Modélisation d''expérience, analyse de données'">math-probastat-modexpananorm__math-stat</xsl:when>
			<xsl:when test="$intitule='Mathématiques/Probabilités et statistiques/Notion de loi à densité: loi uniforme, exponentielle, normale'">math-probastat-notloidens__math-stat</xsl:when>
			<xsl:when test="$intitule='Mathématiques/Probabilités et statistiques/Notion de loi à densité: loi uniforme, exponentielle, normale/Espérance d''une variable aléatoire suivant une loi exponentielle'">math-probastat-notloidens-espvaraleaexpo__math-stat</xsl:when>
			<xsl:when test="$intitule='Mathématiques/Probabilités et statistiques/Notion de loi à densité: loi uniforme, exponentielle, normale/Espérance d''une variable aléatoire suivant une loi uniforme'">math-probastat-notloidens-espvaraleauni__math-stat</xsl:when>
			<xsl:when test="$intitule='Mathématiques/Probabilités et statistiques/Notion de loi à densité: loi uniforme, exponentielle, normale/Loi à densité sur un intervalle'">math-probastat-notloidens-loidensint__math-stat</xsl:when>
			<xsl:when test="$intitule='Mathématiques/Probabilités et statistiques/Notion de loi à densité: loi uniforme, exponentielle, normale/Loi exponentielle'">math-probastat-notloidens-loiexpo__math-stat</xsl:when>
			<xsl:when test="$intitule='Mathématiques/Probabilités et statistiques/Notion de loi à densité: loi uniforme, exponentielle, normale/Loi normale centrée réduite N (0,1)'">math-probastat-notloidens-loinormcent__math-stat</xsl:when>
			<xsl:when test="$intitule='Mathématiques/Probabilités et statistiques/Notion de loi à densité: loi uniforme, exponentielle, normale/Loi normale N (μ, σ) d''espérance μ et d''écart-type σ'">math-probastat-notloidens-loinormesp__math-stat</xsl:when>
			<xsl:when test="$intitule='Mathématiques/Probabilités et statistiques/Notion de loi à densité: loi uniforme, exponentielle, normale/Loi uniforme sur [a, b]'">math-probastat-notloidens-loiunifab__math-stat</xsl:when>
			<xsl:when test="$intitule='Mathématiques/Probabilités et statistiques/Notion de loi à densité: loi uniforme, exponentielle, normale/Théorème de Moivre Laplace (admis)'">math-probastat-notloidens-theomoivr__math-stat</xsl:when>
			<xsl:when test="$intitule='Mathématiques/Probabilités et statistiques/Probabilité sur un ensemble fini'">math-probastat-probfin__math-stat</xsl:when>
			<xsl:when test="$intitule='Mathématiques/Probabilités et statistiques/Probabilité sur un ensemble fini/Probabilité d''un événement'">math-probastat-probfin-probeven__math-stat</xsl:when>
			<xsl:when test="$intitule='Mathématiques/Probabilités et statistiques/Probabilité sur un ensemble fini/Réunion et intersection de deux événements (formule)'">math-probastat-probfin-reuninterseven__math-stat</xsl:when>
			<xsl:when test="$intitule='Mathématiques/Probabilités et statistiques/Probabilités'">math-probastat-probabilites__math-stat</xsl:when>
			<xsl:when test="$intitule='Mathématiques/Probabilités et statistiques/Probabilités/Espérance, variance et écart-type'">math-probastat-probabilites-espvarecartype__math-stat</xsl:when>
			<xsl:when test="$intitule='Mathématiques/Probabilités et statistiques/Probabilités/Loi de probabilité'">math-probastat-probabilites-loiproba__math-stat</xsl:when>
			<xsl:when test="$intitule='Mathématiques/Probabilités et statistiques/Probabilités/Variable aléatoire discrète'">math-probastat-probabilites-varaleadisc__math-stat</xsl:when>
			<xsl:when test="$intitule='Mathématiques/Probabilités et statistiques/Statistique descriptive: variance, écart-type'">math-probastat-statdesc__math-stat</xsl:when>
			<xsl:when test="$intitule='Mathématiques/Probabilités et statistiques/Statistique descriptive: variance, écart-type/Caractéristiques de position et de dispersion'">math-probastat-statdesc-caracposdisp__math-stat</xsl:when>
			<xsl:when test="$intitule='Mathématiques/Probabilités et statistiques/Statistique descriptive: variance, écart-type/médiane, quartiles'">math-probastat-statdesc-mediquart__math-stat</xsl:when>
			<xsl:when test="$intitule='Mathématiques/Probabilités et statistiques/Statistique descriptive: variance, écart-type/moyenne'">math-probastat-statdesc-moyenne__math-stat</xsl:when>
			<xsl:when test="$intitule='Physique'">phys</xsl:when>
			<xsl:when test="$intitule='Physique/Agir : défis du XXIe siècle'">phys-agdefis</xsl:when>
			<xsl:when test="$intitule='Physique/Agir : défis du XXIe siècle/Convertir l''énergie et économiser les ressources'">phys-agdefis-convenerecores__phys-electrostat</xsl:when>
			<xsl:when test="$intitule='Physique/Agir : défis du XXIe siècle/Convertir l''énergie et économiser les ressources/conversion d''énergie: conservation/dégradation'">phys-agdefis-convenerecores-convenergcons__phys-electrostat</xsl:when>
			<xsl:when test="$intitule='Physique/Agir : défis du XXIe siècle/Convertir l''énergie et économiser les ressources/Loi d''Ohm. Effet Joule'">phys-agdefis-convenerecores-loiohmefjoul__phys-electrostat</xsl:when>
			<xsl:when test="$intitule='Physique/Agir : défis du XXIe siècle/Convertir l''énergie et économiser les ressources/notion de rendement de conversion'">phys-agdefis-convenerecores-notrendconv__phys-electrostat</xsl:when>
			<xsl:when test="$intitule='Physique/Agir : défis du XXIe siècle/Convertir l''énergie et économiser les ressources/production de l''énergie électrique: ordre de grandeur de puissances'">phys-agdefis-convenerecores-prodenerelec__phys-electrostat</xsl:when>
			<xsl:when test="$intitule='Physique/Agir : défis du XXIe siècle/Convertir l''énergie et économiser les ressources/puissance et énergie'">phys-agdefis-convenerecores-puissener__phys-electrostat</xsl:when>
			<xsl:when test="$intitule='Physique/Agir : défis du XXIe siècle/Convertir l''énergie et économiser les ressources/ressources énergétiques renouvelables ou non'">phys-agdefis-convenerecores-resenerenouv__phys-electrostat</xsl:when>
			<xsl:when test="$intitule='Physique/Agir : défis du XXIe siècle/Convertir l''énergie et économiser les ressources/transport et stockage de l''énergie'">phys-agdefis-convenerecores-transpstockener__phys-electrostat</xsl:when>
			<xsl:when test="$intitule='Physique/Agir : défis du XXIe siècle/Créer et innover'">phys-agdefis-creerinnov</xsl:when>
			<xsl:when test="$intitule='Physique/Agir : défis du XXIe siècle/Transmettre et stocker de l''information'">phys-agdefis-transtockinfo</xsl:when>
			<xsl:when test="$intitule='Physique/Agir : défis du XXIe siècle/Transmettre et stocker de l''information/caractéristiques d''une image numérique: pixellisation, codage RVB et niveaux de gris'">phys-agdefis-transtockinfo-caracimnum</xsl:when>
			<xsl:when test="$intitule='Physique/Agir : défis du XXIe siècle/Transmettre et stocker de l''information/chaîne de transmission d''informations'">phys-agdefis-transtockinfo-chtransinfo</xsl:when>
			<xsl:when test="$intitule='Physique/Agir : défis du XXIe siècle/Transmettre et stocker de l''information/débit binaire Atténuations'">phys-agdefis-transtockinfo-debbinatt</xsl:when>
			<xsl:when test="$intitule='Physique/Agir : défis du XXIe siècle/Transmettre et stocker de l''information/échantillonnage, quantification, numérisation'">phys-agdefis-transtockinfo-echquannum</xsl:when>
			<xsl:when test="$intitule='Physique/Agir : défis du XXIe siècle/Transmettre et stocker de l''information/signal analogique et numérique'">phys-agdefis-transtockinfo-signoptcapstock</xsl:when>
			<xsl:when test="$intitule='Physique/Agir : défis du XXIe siècle/Transmettre et stocker de l''information/stockage optique - capacité de stockage'">phys-agdefis-transtockinfo-stockoptcap</xsl:when>
			<xsl:when test="$intitule='Physique/Agir : défis du XXIe siècle/Transmettre et stocker de l''information/types de transmission'">phys-agdefis-transtockinfo-typtransm</xsl:when>
			<xsl:when test="$intitule='Physique/Comprendre : lois et modèles'">phys-comploimod</xsl:when>
			<xsl:when test="$intitule='Physique/Comprendre : lois et modèles/Champs et forces'">phys-comploimod-chaforces__phys-magnetism</xsl:when>
			<xsl:when test="$intitule='Physique/Comprendre : lois et modèles/Champs et forces/champ de gravitation'">phys-comploimod-chaforces-chagravit__phys-magnetism</xsl:when>
			<xsl:when test="$intitule='Physique/Comprendre : lois et modèles/Champs et forces/champ de pesanteur local'">phys-comploimod-chaforces-chapesloc__phys-magnetism</xsl:when>
			<xsl:when test="$intitule='Physique/Comprendre : lois et modèles/Champs et forces/champ électrostatique'">phys-comploimod-chaforces-chaelecstat__phys-magnetism</xsl:when>
			<xsl:when test="$intitule='Physique/Comprendre : lois et modèles/Champs et forces/champ magnétique terrestre'">phys-comploimod-chaforces-chamagnterr__phys-magnetism</xsl:when>
			<xsl:when test="$intitule='Physique/Comprendre : lois et modèles/Champs et forces/champ scalaire'">phys-comploimod-chaforces-chascalaire__phys-magnetism</xsl:when>
			<xsl:when test="$intitule='Physique/Comprendre : lois et modèles/Champs et forces/champ vectoriel'">phys-comploimod-chaforces-chavecto__phys-magnetism</xsl:when>
			<xsl:when test="$intitule='Physique/Comprendre : lois et modèles/Cohésion et transformations de la matière'">phys-comploimod-cohetransmat__phys-electromagn</xsl:when>
			<xsl:when test="$intitule='Physique/Comprendre : lois et modèles/Cohésion et transformations de la matière/charge élémentaire e'">phys-comploimod-cohetransmat-charelem__phys-electromagn</xsl:when>
			<xsl:when test="$intitule='Physique/Comprendre : lois et modèles/Cohésion et transformations de la matière/matière à différente échelle: les ordres de grandeur'">phys-comploimod-cohetransmat-matdiffech__phys-physnucl</xsl:when>
			<xsl:when test="$intitule='Physique/Comprendre : lois et modèles/Cohésion et transformations de la matière/particules élémentaires: électrons, neutrons, protons'">phys-comploimod-cohetransmat-partelemelec__phys-physnucl</xsl:when>
			<xsl:when test="$intitule='Physique/Comprendre : lois et modèles/Cohésion et transformations de la matière/radioactivité naturelle et artificielle'">phys-comploimod-cohetransmat-radionatartif__phys-physnucl</xsl:when>
			<xsl:when test="$intitule='Physique/Comprendre : lois et modèles/Énergie, matière et rayonnement'">phys-comploimod-enermatrayo__phys-thermodyn</xsl:when>
			<xsl:when test="$intitule='Physique/Comprendre : lois et modèles/Énergie, matière et rayonnement/bilan d''énergie'">phys-comploimod-enermatrayo-bilenergie__phys-thermodyn</xsl:when>
			<xsl:when test="$intitule='Physique/Comprendre : lois et modèles/Énergie, matière et rayonnement/constante d''Avogadro'">phys-comploimod-enermatrayo-constavo__phys-thermodyn</xsl:when>
			<xsl:when test="$intitule='Physique/Comprendre : lois et modèles/Énergie, matière et rayonnement/dualité onde/particule'">phys-comploimod-enermatrayo-dualondpart__phys-thermodyn</xsl:when>
			<xsl:when test="$intitule='Physique/Comprendre : lois et modèles/Énergie, matière et rayonnement/énergie interne; capacité thermique'">phys-comploimod-enermatrayo-enerintcapther__phys-thermodyn</xsl:when>
			<xsl:when test="$intitule='Physique/Comprendre : lois et modèles/Énergie, matière et rayonnement/flux thermique'">phys-comploimod-enermatrayo-fluxtherm__phys-thermodyn</xsl:when>
			<xsl:when test="$intitule='Physique/Comprendre : lois et modèles/Énergie, matière et rayonnement/mouvement brownien'">phys-comploimod-enermatrayo-mouvbrow__phys-thermodyn</xsl:when>
			<xsl:when test="$intitule='Physique/Comprendre : lois et modèles/Énergie, matière et rayonnement/ordres de grandeur domaines micro/macroscopiques'">phys-comploimod-enermatrayo-ordgranddom__phys-pysnucl</xsl:when>
			<xsl:when test="$intitule='Physique/Comprendre : lois et modèles/Énergie, matière et rayonnement/principe du laser'">phys-comploimod-enermatrayo-princlaser__phys-optiphyssp</xsl:when>
			<xsl:when test="$intitule='Physique/Comprendre : lois et modèles/Énergie, matière et rayonnement/transfert thermique/travail/irréversibilité'">phys-comploimod-enermatrayo-transthertrav__phys-thermodyn</xsl:when>
			<xsl:when test="$intitule='Physique/Comprendre : lois et modèles/Énergie, matière et rayonnement/transferts thermiques: conduction, convection, rayonnement'">phys-comploimod-enermatrayo-transthercond__phys-thermodyn</xsl:when>
			<xsl:when test="$intitule='Physique/Comprendre : lois et modèles/Énergie, matière et rayonnement/transitions d''énergie: électroniques, vibratoires'">phys-comploimod-enermatrayo-transenerelecvib__phys-thermodyn</xsl:when>
			<xsl:when test="$intitule='Physique/Comprendre : lois et modèles/Formes et principe de conservation de l''énergie'">phys-comploimod-forprinconener__phys-thermodyn</xsl:when>
			<xsl:when test="$intitule='Physique/Comprendre : lois et modèles/Formes et principe de conservation de l''énergie/formes d''énergie'">phys-comploimod-forprinconener-formenerg__phys-thermodyn</xsl:when>
			<xsl:when test="$intitule='Physique/Comprendre : lois et modèles/Formes et principe de conservation de l''énergie/frottements ; transferts thermiques ; dissipation d''énergie.'">phys-comploimod-forprinconener-frottransther__phys-thermodyn</xsl:when>
			<xsl:when test="$intitule='Physique/Comprendre : lois et modèles/Formes et principe de conservation de l''énergie/point matériel en mouvement dans le champ de pesanteur : énergies cinétique, potentielle, mécanique'">phys-comploimod-forprinconener-ptmatmvt__phys-thermodyn</xsl:when>
			<xsl:when test="$intitule='Physique/Comprendre : lois et modèles/Formes et principe de conservation de l''énergie/principe de conservation de l''énergie'">phys-comploimod-forprinconener-princconsener__phys-thermodyn</xsl:when>
			<xsl:when test="$intitule='Physique/Comprendre : lois et modèles/Temps, mouvement et évolution'">phys-comploimod-tempmvtevol__phys-relativite</xsl:when>
			<xsl:when test="$intitule='Physique/Comprendre : lois et modèles/Temps, mouvement et évolution/caractère relatif du temps. notion de temps propre'">phys-comploimod-tempmvtevol-carreltemp__phys-relativite</xsl:when>
			<xsl:when test="$intitule='Physique/Comprendre : lois et modèles/Temps, mouvement et évolution/conservation de la quantité de mouvement d''un système isolé'">phys-comploimod-tempmvtevol-consquanmvtsys__phys-mecapoint</xsl:when>
			<xsl:when test="$intitule='Physique/Comprendre : lois et modèles/Temps, mouvement et évolution/définition du temps atomique.'">phys-comploimod-tempmvtevol-deftpsatom__phys-relativite</xsl:when>
			<xsl:when test="$intitule='Physique/Comprendre : lois et modèles/Temps, mouvement et évolution/description du mouvement d''un point au cours du temps : vecteurs position, vitesse et accélération'">phys-comploimod-tempmvtevol-descmvtpttps__phys-mecapoint</xsl:when>
			<xsl:when test="$intitule='Physique/Comprendre : lois et modèles/Temps, mouvement et évolution/durée propre/durée mesurée'">phys-comploimod-tempmvtevol-durprdurmes__phys-mecapoint</xsl:when>
			<xsl:when test="$intitule='Physique/Comprendre : lois et modèles/Temps, mouvement et évolution/énergie mécanique: transferts énergétiques'">phys-comploimod-tempmvtevol-enermecatran__phys-mecapoint</xsl:when>
			<xsl:when test="$intitule='Physique/Comprendre : lois et modèles/Temps, mouvement et évolution/étude énergétique des oscillations libres d''un système mécanique. Dissipation d''énergie. Facteur de qualité : approche énergétique.'">phys-comploimod-tempmvtevol-etuenroscil__phys-mecapoint</xsl:when>
			<xsl:when test="$intitule='Physique/Comprendre : lois et modèles/Temps, mouvement et évolution/forces non conservatives : exemple des frottements'">phys-comploimod-tempmvtevol-fornoconsex__phys-mecapoint</xsl:when>
			<xsl:when test="$intitule='Physique/Comprendre : lois et modèles/Temps, mouvement et évolution/lois de Kepler'">phys-comploimod-tempmvtevol-loikepler__phys-mecapoint</xsl:when>
			<xsl:when test="$intitule='Physique/Comprendre : lois et modèles/Temps, mouvement et évolution/lois de Newton : principe d''inertie, principe des actions réciproques'">phys-comploimod-tempmvtevol-loinewton__phys-mecapoint</xsl:when>
			<xsl:when test="$intitule='Physique/Comprendre : lois et modèles/Temps, mouvement et évolution/mesure du temps et oscillateur, amortissement'">phys-comploimod-tempmvtevol-mestpsoscilamort__phys-mecapoint</xsl:when>
			<xsl:when test="$intitule='Physique/Comprendre : lois et modèles/Temps, mouvement et évolution/mouvement d''un satellite. Révolution de la Terre autour du Soleil.'">phys-comploimod-tempmvtevol-mvtsatelrevol__phys-mecapoint</xsl:when>
			<xsl:when test="$intitule='Physique/Comprendre : lois et modèles/Temps, mouvement et évolution/référentiel galiléen'">phys-comploimod-tempmvtevol-refgalil__phys-mecapoint</xsl:when>
			<xsl:when test="$intitule='Physique/Comprendre : lois et modèles/Temps, mouvement et évolution/temps et relativité restreinte; Invariance de la vitesse de la lumière.'">phys-comploimod-tempmvtevol-tpsrelrest__phys-relativite</xsl:when>
			<xsl:when test="$intitule='Physique/Comprendre : lois et modèles/Temps, mouvement et évolution/travail d''une force constante'">phys-comploimod-tempmvtevol-travforconst__phys-mecapoint</xsl:when>
			<xsl:when test="$intitule='Physique/L''Univers'">phys-univ</xsl:when>
			<xsl:when test="$intitule='Physique/L''Univers/Les étoiles'">phys-univ-etoiles</xsl:when>
			<xsl:when test="$intitule='Physique/L''Univers/Système solaire'">phys-univ-syssol</xsl:when>
			<xsl:when test="$intitule='Physique/L''Univers/Système solaire/gravitation universelle'">phys-univ-syssol-gravuniv</xsl:when>
			<xsl:when test="$intitule='Physique/L''Univers/Système solaire/pesanteur terrestre/attraction terrestre'">phys-univ-syssol-pesteratrter</xsl:when>
			<xsl:when test="$intitule='Physique/L''Univers/Une première présentation de l''Univers'">phys-univ-presunivers</xsl:when>
			<xsl:when test="$intitule='Physique/L''Univers/Une première présentation de l''Univers/évaluer les ordres de grandeurs'">phys-univ-presunivers-evalordgran</xsl:when>
			<xsl:when test="$intitule='Physique/L''Univers/Une première présentation de l''Univers/vitesse de la lumière dans le vide et dans l''air'">phys-univ-presunivers-vitlumvide__phys-optiondul</xsl:when>
			<xsl:when test="$intitule='Physique/La pratique du sport'">phys-pratsport</xsl:when>
			<xsl:when test="$intitule='Physique/La pratique du sport/Étude du mouvement'">phys-pratsport-etumouv__phys-mecapoint</xsl:when>
			<xsl:when test="$intitule='Physique/La pratique du sport/Étude du mouvement/actions mécaniques, modélisation par une force'">phys-pratsport-etumouv-actmecamodfor__phys-mecapoint</xsl:when>
			<xsl:when test="$intitule='Physique/La pratique du sport/Étude du mouvement/effets d''une force sur le mouvement d''un corps: modification de la vitesse et de la trajectoire'">phys-pratsport-etumouv-effforcmvt__phys-mecapoint</xsl:when>
			<xsl:when test="$intitule='Physique/La pratique du sport/Étude du mouvement/mesure de durée chronométrage'">phys-pratsport-etumouv-mesdurchro__phys-mecapoint</xsl:when>
			<xsl:when test="$intitule='Physique/La pratique du sport/Étude du mouvement/référentiel trajectoire'">phys-pratsport-etumouv-reftraj__phys-mecapoint</xsl:when>
			<xsl:when test="$intitule='Physique/La pratique du sport/Étude du mouvement/relativité du mouvement'">phys-pratsport-etumouv-relatmvt__phys-mecapoint</xsl:when>
			<xsl:when test="$intitule='Physique/La pratique du sport/Étude du mouvement/rôle de la masse d''un corps. principe d''inertie'">phys-pratsport-etumouv-rolmascoprs__phys-mecapoint</xsl:when>
			<xsl:when test="$intitule='Physique/La pratique du sport/La pression'">phys-pratsport-pression</xsl:when>
			<xsl:when test="$intitule='Physique/La pratique du sport/La pression/pression dans un liquide'">phys-pratsport-pression-presliq</xsl:when>
			<xsl:when test="$intitule='Physique/La santé'">phys-sante</xsl:when>
			<xsl:when test="$intitule='Physique/La santé/Diagnostic médical'">phys-sante-diagmedic__phys-optiondul</xsl:when>
			<xsl:when test="$intitule='Physique/La santé/Diagnostic médical/domaine de fréquences'">phys-sante-diagmedic-domfreq__phys-optiondul</xsl:when>
			<xsl:when test="$intitule='Physique/La santé/Diagnostic médical/fréquence'">phys-sante-diagmedic-frequ__phys-optiondul</xsl:when>
			<xsl:when test="$intitule='Physique/La santé/Diagnostic médical/période'">phys-sante-diagmedic-periode__phys-optiondul</xsl:when>
			<xsl:when test="$intitule='Physique/La santé/Diagnostic médical/réfraction et réflexion totale'">phys-sante-diagmedic-refrrefl__phys-optiondul</xsl:when>
			<xsl:when test="$intitule='Physique/La santé/Diagnostic médical/signaux périodiques'">phys-sante-diagmedic-signperiod__phys-optiondul</xsl:when>
			<xsl:when test="$intitule='Physique/La santé/Diagnostic médical/tension maximale, tension minimale'">phys-sante-diagmedic-tensmaxmin__phys-optiondul</xsl:when>
			<xsl:when test="$intitule='Physique/La santé/Diagnostic médical/vitesse de la lumière dans le vide et dans l''air'">phys-sante-diagmedic-vitlumvideair__phys-optiondul</xsl:when>
			<xsl:when test="$intitule='Physique/notions pouvant êtres étudiés dans plusieurs thématiques'">phys-notth</xsl:when>
			<xsl:when test="$intitule='Physique/notions vues aussi en chimie'">phys-notch</xsl:when>
			<xsl:when test="$intitule='Physique/Observer : couleur et images, ondes et matière'">phys-obs</xsl:when>
			<xsl:when test="$intitule='Physique/Observer : couleur et images, ondes et matière/Caractéristiques et propriétés des ondes'">phys-obs-caracpropond__phys-optiondul</xsl:when>
			<xsl:when test="$intitule='Physique/Observer : couleur et images, ondes et matière/Caractéristiques et propriétés des ondes/effet Doppler'">phys-obs-caracpropond-effdopl__phys-optiondul</xsl:when>
			<xsl:when test="$intitule='Physique/Observer : couleur et images, ondes et matière/Caractéristiques et propriétés des ondes/interférences'">phys-obs-caracpropond-interfe__phys-optiondul</xsl:when>
			<xsl:when test="$intitule='Physique/Observer : couleur et images, ondes et matière/Couleur, vision et image'">phys-obs-coulvisim__phys-optigeom</xsl:when>
			<xsl:when test="$intitule='Physique/Observer : couleur et images, ondes et matière/Couleur, vision et image/absorption, diffusion, transmission'">phys-obs-coulvisim-absdiftran__phys-optigeom</xsl:when>
			<xsl:when test="$intitule='Physique/Observer : couleur et images, ondes et matière/Couleur, vision et image/accommodation du cristallin'">phys-obs-coulvisim-accomcris__phys-optigeom</xsl:when>
			<xsl:when test="$intitule='Physique/Observer : couleur et images, ondes et matière/Couleur, vision et image/fonctionnements comparé de l''œil et de l''appareil photo'">phys-obs-coulvisim-fonccompoeil__phys-optigeom</xsl:when>
			<xsl:when test="$intitule='Physique/Observer : couleur et images, ondes et matière/Couleur, vision et image/l’œil: modèle de l’œil réduit'">phys-obs-coulvisim-oeilmodred__phys-optigeom</xsl:when>
			<xsl:when test="$intitule='Physique/Observer : couleur et images, ondes et matière/Couleur, vision et image/lentille mince convergente: image réelle et virtuelle'">phys-obs-coulvisim-lenminconv__phys-optigeom</xsl:when>
			<xsl:when test="$intitule='Physique/Observer : couleur et images, ondes et matière/Couleur, vision et image/lentille mince: distance focale, vergence'">phys-obs-coulvisim-lenmindist__phys-optigeom</xsl:when>
			<xsl:when test="$intitule='Physique/Observer : couleur et images, ondes et matière/Couleur, vision et image/lentille mince: relation de conjugaison, grandissement'">phys-obs-coulvisim-lenminrel__phys-optigeom</xsl:when>
			<xsl:when test="$intitule='Physique/Observer : couleur et images, ondes et matière/Couleur, vision et image/vision des couleurs et trichromie. Daltonisme'">phys-obs-coulvisim-viscoultrich__phys-optigeom</xsl:when>
			<xsl:when test="$intitule='Physique/Observer : couleur et images, ondes et matière/Ondes et particules'">phys-obs-ondpartic__phys-optiondul</xsl:when>
			<xsl:when test="$intitule='Physique/Observer : couleur et images, ondes et matière/Ondes et particules/ondes dans la matière: houle, ondes sismiques, ondes sonores'">phys-obs-ondpartic-ondmathoule__phys-optiondul</xsl:when>
			<xsl:when test="$intitule='Physique/Observer : couleur et images, ondes et matière/Ondes et particules/rayonnement dans l''univers'">phys-obs-ondpartic-rayunivers__phys-optiondul</xsl:when>
			<xsl:when test="$intitule='Physique/Observer : couleur et images, ondes et matière/Sources de lumière colorée'">phys-obs-sourlumcol__phys-optiphyssp</xsl:when>
			<xsl:when test="$intitule='Physique/Observer : couleur et images, ondes et matière/Sources de lumière colorée/domaine visible; rayonnement infrarouge et ultraviolet'">phys-obs-sourlumcol-domvisray__phys-optiphyssp</xsl:when>
			<xsl:when test="$intitule='Physique/Observer : couleur et images, ondes et matière/Sources de lumière colorée/loi de Wien'">phys-obs-sourlumcol-loiwien__phys-optiphyssp</xsl:when>
			<xsl:when test="$intitule='Physique/Observer : couleur et images, ondes et matière/Sources de lumière colorée/quantification des niveaux d''énergie de la matière'">phys-obs-sourlumcol-quannivener__phys-optiphyssp</xsl:when>
			<xsl:when test="$intitule='Physique/Observer : couleur et images, ondes et matière/Sources de lumière colorée/relation ∆E=hν dans les échanges d''énergie'">phys-obs-sourlumcol-relechenerg__phys-optiphyssp</xsl:when>
			<xsl:when test="$intitule='Sciences de la vie'">scvie__bio-biolo</xsl:when>
			<xsl:when test="$intitule='Sciences de la vie/Enjeux planétaires contemporains'">scvie-plancont__bio-bioveget</xsl:when>
			<xsl:when test="$intitule='Sciences de la vie/Enjeux planétaires contemporains/La plante domestiquée'">scvie-plancont-plantdom__bio-bioveget</xsl:when>
			<xsl:when test="$intitule='Sciences de la vie/Enjeux planétaires contemporains/Le soleil: une source d''énergie essentielle: la photosynthèse'">scvie-plancont-solsourener__bio-bioveget</xsl:when>
			<xsl:when test="$intitule='Sciences de la vie/Enjeux planétaires contemporains/Nourrir l''humanité: production végétale, animale et pratiques alimentaires'">scvie-plancont-nourrhuman__bio-bioveget</xsl:when>
			<xsl:when test="$intitule='Sciences de la vie/Enjeux planétaires contemporains/Le sol, un patrimoine durable'">scvie-plancont-solpatdur__bio-bioveget</xsl:when>
			<xsl:when test="$intitule='Sciences de la vie/La Terre dans l''Univers, la vie et l''évolution du vivant'">scvie-terunivers</xsl:when>
			<xsl:when test="$intitule='Sciences de la vie/La Terre dans l''Univers, la vie et l''évolution du vivant/Nature du vivant: cellule, ADN, génome'">scvie-terunivers-natvivant</xsl:when>
			<xsl:when test="$intitule='Sciences de la vie/La Terre dans l''Univers, la vie et l''évolution du vivant/Biodiversité, résultat et étape de l''évolution'">scvie-terunivers-biodivresult</xsl:when>
			<xsl:when test="$intitule='Sciences de la vie/La Terre dans l''Univers, la vie et l''évolution du vivant/Génétique et évolution'">scvie-terunivers-genetevolu</xsl:when>
			<xsl:when test="$intitule='Sciences de la vie/La Terre dans l''Univers, la vie et l''évolution du vivant/Expression, transmission et variation de l''information génétique'">scvie-terunivers-exprtravarinfo</xsl:when>
			<xsl:when test="$intitule='Sciences de la vie/La Terre dans l''Univers, la vie et l''évolution du vivant/Un exemple de vie fixée: plantes: structure et physiologie'">scvie-terunivers-exemviefix</xsl:when>
			<xsl:when test="$intitule='Sciences de la vie/Corps humain et santé'">scvie-corpshum</xsl:when>
			<xsl:when test="$intitule='Sciences de la vie/Corps humain et santé/L''exercice physique et la santé : modifications physiologiques, régulations nerveuses, accidents, dopage'">scvie-corpshum-exephyssante</xsl:when>
			<xsl:when test="$intitule='Sciences de la vie/Corps humain et santé/Féminin, masculin: procréation et sexualité'">scvie-corpshum-femmascproc</xsl:when>
			<xsl:when test="$intitule='Sciences de la vie/Corps humain et santé/Variation génétique et santé'">scvie-corpshum-vargensante</xsl:when>
			<xsl:when test="$intitule='Sciences de la vie/Corps humain et santé/De l''oeil au cerveau: quelques aspects de la vision'">scvie-corpshum-oeilcervvis</xsl:when>
			<xsl:when test="$intitule='Sciences de la vie/Corps humain et santé/L''organisme face aux dangers: quelques aspects de la réaction immunitaire'">scvie-corpshum-orgdangers</xsl:when>
			<xsl:when test="$intitule='Sciences de la vie/Corps humain et santé/Un paramètre physiologique sous surveillance: la pression artérielle'">scvie-corpshum-paramphysurv</xsl:when>
			<xsl:when test="$intitule='Sciences de la vie/Corps humain et santé/Neurone et fibre musculaire: le mouvement et sa commande nerveuse'">scvie-corpshum-neurfibmusc</xsl:when>


<!--A COMPLETER-->
<!--			<xsl:when test="$intitule='Défaut pour LISCINUM2017/Bases de l’EEEA 1 : Electricité/Ch1_Généralités en électricité'">aaaa__bbbb</xsl:when>-->
			
			<xsl:otherwise>XX_CODE_INCONNU_<xsl:value-of select="$intitule"/></xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	
</xsl:stylesheet>
